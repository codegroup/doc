### cyberporto-doc ###

System administration and development documentation, covers
installation and configuration of OpenBSD and CRUX Gnu/Linux
system.

Covers GNU/Linux and OpenBSD tools related with services such
as email, web server, etc.

https://doc.cyberporto.xyz

### Some Notes ###

* This is not a "Handbook for OpenBSD" substitution.
* This is not a "Handbook for CRUX" substitution.
* Not meant to be "full manual".

### Configuration and Ports ###

* Only ports required during "core" installation.
* Less personalized ports the better.
* Less changes to configuration files the better.
* Less "packages" installed the better.
* Only free software, (except the kernel firmware, linux-libre port available)

### Contribution guidelines ###

* Keep html source minimal and readable.
* Contribute to this document :)

### Who do I talk to? ###

* Everyone including me.

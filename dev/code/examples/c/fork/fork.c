
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

int main(){ 

	pid_t cpid,ppid;
	char errbuf[1024];

	cpid = fork();
	if (cpid == -1) {
		(void) snprintf(errbuf, sizeof(errbuf),
				"fork: %s", strerror(errno));
		printf("%s\n", errbuf);
		exit(1);
	}

	if (cpid == 0) {	
		//child
		ppid = getppid();
		if(ppid == 1){
			printf("parent died ?\n");
			_exit(1);
		}
		printf("I'm child with  %i, parent %i\n", getpid(), ppid);
		_exit(0);
	}
/* parent */
	wait(NULL);
	printf("Child id: %i\n", cpid);
	return 0;
}


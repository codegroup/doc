#include <json-c/json.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {

    json_object *json_obj=NULL;
    json_object *rate=NULL;
    json_object *token_obj=NULL;
    const char *symbol=NULL;
    const char *token=NULL;
    size_t sym_size=0;

    if(argc < 2) {
	printf("Usage: %s <SYMBOL>\n", argv[0]);
	return 1;
    }

    //enum json_type type;

    json_obj = json_object_from_file("page.out");
    rate = json_object_object_get(json_obj, "rates");

    json_object_object_foreach(rate, key, val) {
        if(strncmp(argv[1], key, (size_t)strlen(argv[1])) == 0){
            token_obj=val;
            printf("id:%s\n", key);
            json_object_object_foreach(token_obj, key, val) {
                printf("%s:%s\n", key, json_object_get_string(val));
            }
        }
    }
    return 0;
}

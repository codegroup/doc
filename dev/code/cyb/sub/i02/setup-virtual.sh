#!/bin/sh

# First we define the function
ConfirmOrExit ()
{
    while true
    do
        echo -n "Please confirm (y or n) :"
        read CONFIRM
        case $CONFIRM in
            y|Y|YES|yes|Yes) break ;;
            n|N|no|NO|No)
                echo "Aborting - you entered $CONFIRM"
                exit
                ;;
            *) echo "Please enter only y or n"
        esac
    done
    echo "You entered $CONFIRM. Continuing ..."
}

DEV_NAME=${1}
IMG=${2}.qcow2
SIZE=${3}
CHROOT="/mnt"
DEV="/dev/${DEV_NAME}"

echo "/srv/qemu/img/${IMG}"
echo "${SIZE}"
echo "DEV_NAME=${DEV_NAME}"
echo "DEV=${DEV}"
echo "CHROOT=${CHROOT}"

ConfirmOrExit

#qemu-img create -f qcow2 example.qcow2 20G
qemu-img create -f qcow2 /srv/qemu/img/${IMG} ${SIZE}
qemu-nbd -c ${DEV} /srv/qemu/img/${IMG}

parted --script ${DEV} \
    mklabel gpt \
    unit mib \
    mkpart primary 2 4 \
    name 1 grub \
    mkpart ESP fat32 4 128 \
    name 2 efi \
    mkpart primary ext4 128 1128 \
    name 3 boot \
    mkpart primary ext4 1128 12128 \
    name 4 root \
    mkpart primary ext4 12128 14128 \
    name 5 var \
    mkpart primary ext4 14128 100% \
    name 6 lvm \
    set 1 bios_grub on \
    set 2 boot on \
    set 6 lvm on

kpartx -a -s -l -u ${DEV}

mkfs.fat -F 32  /dev/mapper/${DEV_NAME}p2
mkfs.ext4       /dev/mapper/${DEV_NAME}p3
mkfs.ext4       /dev/mapper/${DEV_NAME}p4
mkfs.ext4       /dev/mapper/${DEV_NAME}p5
pvcreate	/dev/mapper/${DEV_NAME}p6

mount /dev/mapper/${DEV_NAME}p4 $CHROOT
mkdir -p $CHROOT/proc
mkdir -p $CHROOT/sys
mkdir -p $CHROOT/dev
mkdir -p $CHROOT/media

mkdir -p $CHROOT/boot
mount /dev/mapper/${DEV_NAME}p3 $CHROOT/boot
mkdir -p $CHROOT/boot/efi
mount /dev/mapper/${DEV_NAME}p2 $CHROOT/boot/efi
mkdir -p $CHROOT/var
mount /dev/mapper/${DEV_NAME}p5 $CHROOT/var

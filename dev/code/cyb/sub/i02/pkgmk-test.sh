#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CONF=${DIR}/pkgmk-test.conf
echo "pkgmk -cf $CONF -d -is $1"
fakeroot pkgmk -cf $CONF -d -is $1

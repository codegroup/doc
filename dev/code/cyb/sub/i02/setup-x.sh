#!/bin/bash

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

DIR=$(dirname "$SCRIPTPATH");
DIR_CONF=$DIR"/conf"



prt-get depinst xorg-server \
	xorg-xinit \
	xorg-xrdb \
	xorg-xdpyinfo \
	xorg-xauth \
	xorg-xmodmap \
	xorg-xrandr \
	xorg-xgamma \
	xorg-xf86-input-evdev \
	xorg-xf86-input-synaptics \
	xsel \
	xkeyboard-config

prt-get depinst xorg-font-util \
	xorg-font-alias \
	xorg-font-dejavu-ttf \
	xorg-font-cursor-misc \
	xorg-font-misc-misc \
	console-font-terminus \
	xorg-font-terminus \
	xorg-font-mutt-misc

prt-get search xorg-font-bitstream | xargs sudo prt-get depinst
prt-get search xorg-font-bh | xargs sudo prt-get depinst

#prt-get search otf- | xargs sudo prt-get depinst
#prt-get depinst otf-sourcecode

prt-get depinst \
            alsa-utils \
            libdrm \
            mesa3d \
            ffmpeg \
            gstreamer \
            gstreamer-vaapi \
            gst-plugins-base \
            gst-plugins-good \
            gst-plugins-bad \
            gst-plugins-ugly \
            cmus \
            dmenu \
            st \
            gparted \
            gimp \
            libreoffice \
            ca-certificates \
            linux-pam \
            gstreamer \
            libgd \
            icu \
            syndaemon \
	    firefox

prt-get depinst \
	openbox \
	dwm \
       	spectrwm \
	mate

ConfirmOrExit () {
    while true
    do
        echo -n "Please confirm (y or n) :"
        read CONFIRM
        case $CONFIRM in
            y|Y|YES|yes|Yes) break ;;
            n|N|no|NO|No)
                echo "Aborting - you entered $CONFIRM"
                exit
                ;;
            *) echo "Please enter only y or n"
        esac
    done
    echo "You entered $CONFIRM. Continuing ..."
}

echo "SCRIPT=$SCRIPT";
echo "SCRIPTPATH=$SCRIPTPATH";
echo "DIR=$DIR";
echo "DIR_CONF=$DIR_CONF";
ConfirmOrExit

cp -R $DIR_CONF/etc/X11/* /etc/X11/


#!/bin/bash

CHROOT="/mnt"

mkdir -p $CHROOT/boot/efi
mkdir -p $CHROOT/media

mkdir -p $CHROOT/dev
mkdir -p $CHROOT/tmp
mkdir -p $CHROOT/proc
mkdir -p $CHROOT/sys

mount -t proc proc $CHROOT/proc
mount -t sysfs sys $CHROOT/sys
mount --bind /dev $CHROOT/dev

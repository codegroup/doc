#!/bin/sh

# find and replace string on files
folder=$1
oldstring=$2
newstring=$3

#grep -rl $oldstring $folder | xargs sed -i s@$oldstring@$newstring@g
grep -rl "$oldstring" $folder | xargs sed -i "s@$oldstring@$newstring@g"

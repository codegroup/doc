#!/bin/bash

ROOT_DIR=
DEST_DIR=/root/backup
DEST_SYS="${DEST_DIR}/system"
DATA_CNF="${DEST_DIR}/conf"
DATA_USR="${DEST_DIR}/user"
DATA_SRV="${DEST_DIR}/srv"

ConfirmOrExit ()
{
    while true
    do
        echo -n "Please confirm (y or n) :"
        read CONFIRM
        case $CONFIRM in
            y|Y|YES|yes|Yes) break ;;
            n|N|no|NO|No)
                echo "Aborting - you entered $CONFIRM"
                exit
                ;;
	*) echo "Please enter only y or n"
esac
done
echo "You entered $CONFIRM. Continuing ..."
}

print_data () {
    echo "ROOT_DIR=${ROOT_DIR}"
    echo "DEST_DIR=${DEST_DIR}"
    echo "DEST_SYS=${DEST_SYS}"
    echo "DATA_CNF=${DATA_CNF}"
    echo "DATA_USR=${DATA_USR}"
    echo "DATA_SRV=${DATA_SRV}"
}

print_help() {
    echo "usage: backup-system [options]"
    echo "options:"
    echo "  -r,   --root                root directory to backup, default /"
    echo "  -d,   --destination         save backup, default /root/backup"
    echo "  -h,   --help                print help and exit"
}

while [ "$1" ]; do
    case $1 in
        -r|--root)
            ROOT_DIR=$2
            if [ ${ROOT_DIR} == "/" ]; then
                ROOT_DIR=""
            fi
            shift ;;
        -d|--destination)
            DEST_DIR=$2

            # Destination directory
	    DEST_SYS="${DEST_DIR}/system"
	    DATA_CNF="${DEST_DIR}/conf"
	    DATA_USR="${DEST_DIR}/user"
	    DATA_SRV="${DEST_DIR}/srv"

            shift ;;
        -h|--help)
            print_help
            exit 0 ;;
        *)
            echo "backup-system: invalid option $1"
            print_help
            exit 1 ;;
    esac
    shift
done

print_data
ConfirmOrExit

mkdir -p ${DATA_CNF}
mkdir -p ${DATA_USR}
mkdir -p ${DATA_SRV}

# Backup system settings
tar --xattrs -zcpf $DATA_CNF/etc.tar.gz \
    --directory=$ROOT_DIR/etc \
    .

tar --xattrs -zcpf $DATA_CNF/usr_etc.tar.gz \
    --directory=$ROOT_DIR/usr/etc \
    .

bacup_home_metadata () {
# User Meta Data

    for dir in /home/*; do
	if [ "${dir}" != "/home/lost+found" ]; then
	    user=$(basename $dir)
	    tar --xattrs -zcpf "${DATA_USR}/meta-${user}.tar.gz" \
		$dir/.bash_profile \
		$dir/.bashrc \
		$dir/.config \
		$dir/.gitconfig \
		$dir/.gnupg \
		$dir/.irssi \
		$dir/.lynxrc \
		$dir/.mutt \
		$dir/.netrc \
		$dir/.profile \
		$dir/.spectrwm.conf \
		$dir/.ssh \
		$dir/.tmux.conf \
		$dir/.vim \
		$dir/.vimrc \
		$dir/.xinitrc

	    # encript data
	    #gpg --output "${DATA_USR}/meta-${user}.tar.gz.gpg" \
		#    --encrypt --recipient user@host \
		#    "${DATA_USR}/meta-${user}.tar.gz"

	    tar --xattrs -zcpf "${DATA_USR}/gitolite-${user}.tar.gz" \
		$dir/gitolite-admin
	fi
    done
}

backup_services () {
    # backup web data first stop php and nginx
    for pkg_www in ${ROOT_DIR}/srv/www/*; do
	if [[ ! $(ls ${pkg_www} | grep -v "backup_deploy") = "" ]]; then
	    pkg_back="${DATA_SRV}/www"
	    if [ ! -d ${pkg_back} ]; then
		mkdir -p ${pkg_back}
	    fi
	    bck_file="${pkg_back}/$(basename ${pkg_www}).tar.gz"
	    exc="${pkg_www}/backup_deploy"
	    tar --exclude ${exc} --xattrs -zcpf ${bck_file} ${pkg_www}
	fi
    done

    # backup database data first dump all databases
    pkg_back="${DATA_SRV}/pgsql"
    if [ ! -d ${pkg_back} ]; then
	mkdir -p ${pkg_back}
    fi
    pg_dumpall -U postgres | gzip > ${pkg_back}/cluster_dump.gz

    tar --xattrs -zcpf "${pkg_back}/pgsql-conf.tar.gz" \
	${ROOT_DIR}/srv/pgsql/data/pg_hba.conf \
	${ROOT_DIR}/srv/pgsql/data/pg_ident.conf \
	${ROOT_DIR}/srv/pgsql/data/postgresql.conf


    # backup gitolite repositories
    pkg_back="${DATA_SRV}/gitolite"
    if [ ! -d ${pkg_back} ]; then
	mkdir -p ${pkg_back}
    fi

    tar --xattrs -zcpf "${pkg_back}/gitolite.tar.gz" \
	--directory=${ROOT_DIR}/srv/gitolite \
	.
}

while true
do
    echo "Backup User Metadata ?"
    echo "Please confirm (y or n): "
    read CONFIRM
    case $CONFIRM in
        n|N|no|NO|No) break ;;
        y|Y|YES|yes|Yes)
            echo "Accept - you entered $CONFIRM"
            bacup_home_metadata
            break
            ;;
        *) echo "Please enter only y or n"
    esac
done

# Server Data
while true
do
    echo "Backup Server Data ?"
    echo "Please confirm (y or n): "
    read CONFIRM
    case $CONFIRM in
        n|N|no|NO|No) break ;;
        y|Y|YES|yes|Yes)
            echo "Accept - you entered $CONFIRM"
            backup_services
            break
            ;;
        *) echo "Please enter only y or n"
    esac
done

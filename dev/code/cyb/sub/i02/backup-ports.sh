#!/bin/bash

DEST_SYS=/usr/ports/releases/stable
DEST_ARC=/usr/ports/releases/archives

#PACKAGES=/usr/ports/packages
PACKAGES=/usr/ports/archive/packages

#PRT_GET_FLAGS="-fr -if -is"
PRT_GET_FLAGS="-fr"

# Build Name = R1 (System number) D1 (Droid/Machine number)
BUILD_NAME="R1D9"
#echo "Give build NAME;"
#echo "${DEST_ARC}/NAME-0.0.0.targ.gz"
#read BUILD_NAME

#internal
database=${DEST_SYS}/pkg-db.tar.gz
ports=${DEST_SYS}/etc-ports.tar.gz
metadata=${DEST_SYS}/metadata.tar.gz
build=${DEST_SYS}/metadata/build-version
portsver=${DEST_SYS}/metadata/ports-releases
all=${DEST_SYS}/metadata/all-installed
collinst=${DEST_SYS}/metadata/installed
notfound=${DEST_SYS}/metadata/not-found

echo "Give build CRUX_RELEASE.VERSION (3.4.X);"
echo "${DEST_ARC}/${BUILD_NAME}-VERSION.tar.gz"
read BUILD_VERSION

mkdir -p ${DEST_SYS}
mkdir -p ${DEST_ARC}
mkdir -p ${DEST_SYS}/metadata

archive="${DEST_ARC}/${BUILD_NAME}-${BUILD_VERSION}.tar" 
echo "Creating $archive  ..."
if [ -f ${archive} ]; then
	rm ${archive}
fi

backup_collections() {

    # backup collection packages
    while read COLL_VERSION; do
	    COLL_NAME=$(echo $COLL_VERSION | cut -d "-" -f 1)
	    COLL_RELEASE=$(echo $COLL_VERSION | cut -d "-" -f 2)

	    if [ -f ${DEST_SYS}/${COLL_VERSION}.tar ]; then
	    	rm ${DEST_SYS}/${COLL_VERSION}.tar
	    fi

	    while read PORT_NAME; do
		    # get installed version not version on ports
		    PACKAGE=$(grep "^${PORT_NAME}#" ${all})

		    # check if binary package exist
		    if [ ! -f /usr/ports/packages/${PACKAGE} ]; then
			    echo "Building package: ${PACKAGE};"
			    sudo prt-get update ${PRT_GET_FLAGS} ${PORT_NAME}
		    fi

		    if [ -f /usr/ports/packages/${PACKAGE} ]; then
			    echo ${PACKAGE} >> ${DEST_SYS}/metadata/${COLL_NAME}-backup
			    tar rvf ${DEST_SYS}/${COLL_VERSION}.tar --directory=${PACKAGES} ${PACKAGE}
		    else
			    echo "Package ${PACKAGE} from ${PORT_NAME} port not found."
			    echo ${PACKAGE} >> ${notfound}-${COLL_VERSION}
		    fi
    	    done < ${collinst}-${COLL_NAME}

	    tar rvf $archive --directory=${DEST_SYS} ${COLL_VERSION}.tar 

    done < ${portsver}
}

create_metadata() {

    echo "${BUILD_NAME}-${BUILD_VERSION}" > ${build}

    # archive pkgutils data
    tar --xattrs -zcpf $database --directory=/var/lib/pkg/ db
    tar rvf $archive --directory=${DEST_SYS} $(basename ${database})

    # archive ports data
    tar --xattrs -zcpf $ports --directory=/etc/ports .
    tar rvf $archive --directory=${DEST_SYS} $(basename ${ports})

    # must be using gwak instead of sed
    prt-get listinst -v | sed 's/ /#/g' | sed 's/$/.pkg.tar.gz/g' > ${all}

    if [ -f ${portsver} ]; then
	    rm ${portsver}
    fi

    for filename in /etc/ports/*.git; do
	source $filename

	echo "${NAME} port collection release (exp; ${BUILD_VERSION}):"

	read RELEASE
	echo ${NAME}-${RELEASE} >> ${portsver}

	# backup ports collection
	echo "Backing up collection: 	${NAME}"
	tar --xattrs -zcpf ${DEST_SYS}/"ports"-${NAME}-${RELEASE}.tar.gz \
		--directory=/usr/ports/${NAME} \
		--exclude=.git \
		.

    	tar rvf $archive --directory=${DEST_SYS} "ports"-${NAME}-${RELEASE}.tar.gz

	# create list of installed packages 
	prt-get printf "%i %p %n\n" | grep "yes /usr/ports/${NAME}" | cut -d " " -f 3 > ${collinst}-${NAME}

done
}

create_archive() {
    tar --xattrs -zcpf ${metadata} --directory=${DEST_SYS} metadata/
    tar rvf $archive --directory=${DEST_SYS} $(basename ${metadata})
}

update_host() {
	echo "Creating links to /usr/ports/installed"
	rm -r /usr/ports/installed
	pkg_installed

	echo "Creating ports page"
	portspage --title=${BUILD_NAME}-${BUILD_VERSION} /usr/ports/installed > /usr/ports/installed/index.html
}

create_metadata
backup_collections
create_archive
update_host

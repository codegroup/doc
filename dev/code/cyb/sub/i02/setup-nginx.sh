#!/bin/sh

#. `dirname $0`/config-install.sh
#
#prt-get depinst nginx
#
#cp -R $CONF_DIR/etc/nginx/* /etc/nginx/
#
#mkdir /srv/www
#chown www:www /srv/www
#
#usermod -a -g www nginx
#usermod -m -d /srv/www nginx

openssl genrsa -des3 -out /etc/ssl/keys/nginx.key 2048
openssl req -new -key /etc/ssl/keys/nginx.key -out /etc/ssl/certs/nginx.csr
openssl x509 -req -days 365 \
            -in /etc/ssl/certs/nginx.csr \
            -signkey /etc/ssl/keys/nginx.key \
            -out /etc/ssl/certs/nginx.crt

cp /etc/ssl/keys/nginx.key /etc/ssl/keys/nginx.key.pass
openssl rsa -in /etc/ssl/keys/nginx.key.pass -out /etc/ssl/keys/nginx.key


#!/bin/bash

# Absolute path to this script,
# e.g. /home/user/doc/core/scripts/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in,
# e.g. /home/user/doc/core/scripts
SCRIPTPATH=$(dirname "$SCRIPT")
# Absolute path to doc section,
# e.g /home/user/doc/core
DIR_DATA=$(dirname "$SCRIPTPATH");

CHROOT="/mnt"
DATA_CNF="${DIR_DATA}/conf"
ADMIN_USER="machine-admin"

ConfirmOrExit()
{
    while true
    do
	echo -n "Please confirm (y or n) :"
	read CONFIRM
	case $CONFIRM in
	    y|Y|YES|yes|Yes) break ;;
	    n|N|no|NO|No)
		echo "Aborting - you entered $CONFIRM"
		exit
		;;
	    *) echo "Please enter only y or n"
	esac
    done
    echo "You entered $CONFIRM. Continuing ..."
}

setup_core() {

    echo "1.1. Copying configuration data;\n"
    if [ -f "${DATA_CNF}/etc.tar.gz" ]; then
	echo "1.1. Extracting configuration data;\n"
	tar --xattrs -xpvf $DATA_CNF/etc.tar.gz --directory=$CHROOT/etc
	tar --xattrs -xpvf $DATA_CNF/usr_etc.tar.gz --directory=$CHROOT/usr/etc
    else
	echo "1.1.7. dns resolver, copy resolv.conf;\n"
	cp /etc/resolv.conf $CHROOT/etc

	echo "1.1.9. Install Skeletons\n"
	cp -r $DATA_CNF/skel $CHROOT/etc/

	cp $DATA_CNF/sudoers $CHROOT/etc/

	echo "1.2.1. Set hostname and hosts;"
	cp $DATA_CNF/hosts $CHROOT/etc/

	echo "1.2.5. File system table;"
	cp $DATA_CNF/fstab $CHROOT/etc/
	chroot $CHROOT /bin/bash -c "mount >> /etc/fstab"

	echo "1.2.6. Initialization Scripts;"
	cp $DATA_CNF/rc.d/* $CHROOT/etc/rc.d/
	cp $DATA_CNF/rc.conf $CHROOT/etc/

	echo "1.3 Install Ports;"
	rm -fr $CHROOT/etc/ports
	cp -r $DATA_CNF/ports $CHROOT/etc/ports
	cp $DATA_CNF/ports.conf $CHROOT/etc/ports.conf

	echo "1.3.3 Configure pkgmk;"
	cp $DATA_CNF/pkgmk.conf $CHROOT/etc/pkgmk.conf
	cp $DATA_CNF/pkgmk.conf.harden $CHROOT/etc/pkgmk.conf.harden

	echo "1.3.4 Configure prt-get;"
	cp $DATA_CNF/prt-get.conf $CHROOT/etc/

    fi

    echo "1.2.2. Set timezone;"
    chroot $CHROOT /bin/bash -c tzselect

    echo "1.2.3. Set locale;"
    chroot $CHROOT /bin/bash -c "localedef -i en_US -f UTF-8 en_US.UTF-8"
}

setup_users(){
    echo "1.2.4.2. Create Administrator $ADMIN_USER;"

    chroot $CHROOT /usr/bin/env -i \
	HOME=/root TERM="$TERM" PS1='\u:\w\$ ' \
	PATH=/bin:/usr/bin:/sbin:/usr/sbin \
	/bin/bash -c "useradd -U -m -k /etc/skel -s /bin/bash $ADMIN_USER"

    echo "1.2.4.3. Add Administrator $ADMIN_USER to wheel group;"
    chroot $CHROOT /bin/bash -c "usermod -a -G wheel $ADMIN_USER"
    echo "1.2.4.3. Uncomment to allow members of group wheel to execute any command\n
    #    %wheel ALL=(ALL) ALL"

    echo "1.3.1. Ports Layout;"

    chroot --userspec=pkgmk:pkgmk $CHROOT /bin/bash -c "mkdir /usr/ports/{work,distfiles,packages,work,pkgbuild}"

    echo "1.3.2. Build as unprivileged user;"


    chroot $CHROOT /usr/bin/env -i \
	HOME=/root TERM="$TERM" PS1='\u:\w\$ ' \
	PATH=/bin:/usr/bin:/sbin:/usr/sbin \
	/bin/bash -c "useradd -U -M -d /usr/ports -s /bin/false pkgmk"

    chroot $CHROOT /usr/bin/env -i \
	HOME=/root TERM="$TERM" PS1='\u:\w\$ ' \
	PATH=/bin:/usr/bin:/sbin:/usr/sbin \
	/bin/bash -c "usermod -a -G pkgmk $ADMIN_USER"

    chroot $CHROOT /usr/bin/env -i \
	HOME=/root TERM="$TERM" PS1='\u:\w\$ ' \
	PATH=/bin:/usr/bin:/sbin:/usr/sbin \
	/bin/bash -c "chown pkgmk /usr/ports/{distfiles,packages,work,pkgbuild}"

    chroot $CHROOT /usr/bin/env -i \
	HOME=/root TERM="$TERM" PS1='\u:\w\$ ' \
	PATH=/bin:/usr/bin:/sbin:/usr/sbin \
	/bin/bash -c "chown pkgmk:pkgmk /usr/ports/pkgbuild"

    chroot $CHROOT /usr/bin/env -i \
	HOME=/root TERM="$TERM" PS1='\u:\w\$ ' \
	PATH=/bin:/usr/bin:/sbin:/usr/sbin \
	/bin/bash -c "chmod g+w /usr/ports/pkgbuild"

    PKGMK_WRK="pkgmk   /usr/ports/work tmpfs size=30G,gid=$(id -g pkgmk),uid=$(id -u pkgmk),defaults,mode=0750 0    0 >> /etc/fstab"

    chroot $CHROOT /usr/bin/env -i \
	HOME=/root TERM="$TERM" PS1='\u:\w\$ ' \
	PATH=/bin:/usr/bin:/sbin:/usr/sbin \
	/bin/bash -c "echo ${PKGMK_WRK} >> /etc/fstab"
}

setup_config(){
    vim $CHROOT/etc/rc.conf
    vim $CHROOT/etc/hosts
    vim $CHROOT/etc/resolv.conf
    vim $CHROOT/etc/fstab

    vim $CHROOT/etc/pkgmk.conf
    vim $CHROOT/etc/prt-get.conf
}

echo "ADMIN_USER=${ADMIN_USER}";
echo "CHROOT=${CHROOT}";
echo "DATA_CNF=${DATA_CNF}";

ConfirmOrExit

setup_core
setup_users
setup_config

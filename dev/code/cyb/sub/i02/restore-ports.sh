#!/bin/bash

BUILD_NAME=R1D9
BUILD_VERSION=3.4.1
WORK_DIR=/usr/ports/work/${BUILD_NAME}-${BUILD_VERSION}

prepare_archive() {
    mkdir -p ${WORK_DIR}
    wget --directory-prefix=${WORK_DIR} https://tribu.semdestino.org/mirror/releases/archives/${BUILD_NAME}-${BUILD_VERSION}.tar.gz
    echo "extracting ${WORK_DIR}/${BUILD_NAME}-${BUILD_VERSION}.tar.gz..."
    tar -C ${WORK_DIR} -xf ${WORK_DIR}/${BUILD_NAME}-${BUILD_VERSION}.tar.gz
    rm ${WORK_DIR}/${BUILD_NAME}-${BUILD_VERSION}.tar.gz
}

extract_archives() {
    while read COLL_VERSION; do
        COLL_NAME=$(echo $COLL_VERSION | cut -d "-" -f 1)
        COLL_RELEASE=$(echo $COLL_VERSION | cut -d "-" -f 2)
        echo "extracting ${COLL_VERSION}.tar to /usr/ports/packages"
        tar -C /usr/ports/packages -xf ${WORK_DIR}/${COLL_VERSION}.tar
        rm ${WORK_DIR}/${COLL_VERSION}.tar
    done < ${WORK_DIR}/metadata/ports-releases
}

update_system() {
    mkdir -p ${WORK_DIR}/etc/ports
    tar -C ${WORK_DIR}/etc/ports -xf ${WORK_DIR}/etc_ports.tar.gz
    cp ${WORK_DIR}/etc/ports/*.git /etc/ports/

    while read COLL_VERSION; do
        COLL_NAME=$(echo $COLL_VERSION | cut -d "-" -f 1)
        ports -u $COLL_NAME
    done < ${WORK_DIR}/metadata/ports-releases

    # first update with prt-get
    prt-get sysup
    prt-get --pre-install --post-install update $(prt-get listinst)
    prt-get depinst $(prt-get printf "%p %n\n" | grep "/usr/ports/core" | cut -d " " -f 2)
    prt-get update $(revdep)

    rejmerge
}

install_distro() {
    while read PACKAGE; do
        prt-get depinst $(echo $PACKAGE | cut -d "#" -f 1);
    done < ${WORK_DIR}/all-installed.pkg
}

prepare_archive
extract_archives
update_system
install_distro

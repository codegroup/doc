#!/bin/sh

setup_5() {
    # setups git and returns;
    # 0 - on success
    # 1 - on failure
    if [ ! -x "$(command -v git)" ]; then
        echo "${RED}[NO] - you need to install git."
        echo $CLS
        return 1;
    fi

    echo "${GRE}[OK] - GIT is ready."
    return 0;
}

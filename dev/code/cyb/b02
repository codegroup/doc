#!/bin/sh

BOT_NAME="B02"
REVISION="A00"
DOC="${HOME}/doc/"

if [ "${USER}" = "" ]; then
    USER=$(whoami)
fi

SYSTEM=$(uname)

DGRE='\033[33;32m'
BLU='\033[33;34m'
RED='\033[33;91m'
GRE='\033[33;92m'
CLS='\033[0m'

SLT_0="0 ssh   "
SLT_1="1 tmux  "
SLT_2="2 ksh   "
SLT_3="3 lynx  "
SLT_4="4 vim   "
SLT_5="5 git   "
SLT_6="6 mutt  "
SLT_7="7 irssi "
SLT_8="${CLS}8 exit  "

. sub/b01/check_tools.sh
. sub/b02/check_tools.sh

check_tools() {

    check_ssh
    if [ "$?" = 0 ]; then
        SLT_0="${GRE}${SLT_0}"
    else
        SLT_0="${RED}${SLT_0}"
    fi

    check_tmux
    if [ "$?" = 0 ]; then
        SLT_1="${GRE}${SLT_1}"
    else
        SLT_1="${RED}${SLT_1}"
    fi

    # check_shell
    SLT_2="${RED}${SLT_2}"

    # check_lynx
    SLT_3="${RED}${SLT_3}"

    # check_vim
    check_vim
    if [ "$?" = 0 ]; then
        SLT_4="${GRE}${SLT_4}"
    else
        SLT_4="${RED}${SLT_4}"
    fi

    check_git
    if [ "$?" = 0 ]; then
        SLT_5="${GRE}${SLT_5}"
    else
        SLT_5="${RED}${SLT_5}"
    fi

    # check_mutt
    SLT_6="${RED}${SLT_6}"

    # check_irssi
    SLT_7="${RED}${SLT_7}"
}

check_doc() {
    CMD=$1
    RET=$2

    if [ "$CMD" = 0 ]; then
        CMD="lynx"
    else
        CMD="vim"
    fi

    case $RET in
        0)
            $CMD ${DOC}/tools/openssh.html
            ;;
        1)
            $CMD ${DOC}/tools/tmux.html
            ;;
        2)
            $CMD ${DOC}/tools/ksh.html
            ;;
        3)
            $CMD ${DOC}/tools/lynx.html
            ;;
        4)
            $CMD ${DOC}/tools/vim.html
            ;;
        5)
            $CMD ${DOC}/dev/git/index.html
            ;;
        6)
            $CMD ${DOC}/tools/mutt.html
            ;;
        7)
            $CMD ${DOC}/tools/irssi.html
            ;;
        *)
    esac

}
setup_tools() {
    RET=$1
    if [ -f "sub/b02/setup_${RET}.sh" ]; then
        . sub/b02/setup_${RET}.sh
        setup_${RET} ${DOC}
    else
        vim sub/b02/setup_${RET}.sh
    fi
}
print_menu() {

    clear
    echo "${GRE}Hello ${CLS}${USER} Commander${GRE},\nwelcome to your ${CLS}${SYSTEM} ${GRE}system !!!\n"

    echo "${BLU}[${SLT_0}${BLU}] [${SLT_3}${BLU}] [${SLT_6}${BLU}]"
    echo "${BLU}[${SLT_1}${BLU}] [${SLT_4}${BLU}] [${SLT_7}${BLU}]"
    echo "${BLU}[${SLT_2}${BLU}] [${SLT_5}${BLU}] [${SLT_8}${BLU}]"
    echo "\n"
}

RET=0
while [ "$RET" != 8 ]
do
    check_tools
    print_menu

    echo "${GRE}Select tool number and ${CLS}press enter.\n"
    read RET

    # exit conditions
    if [ "$RET" == "" ]; then
        exit
    fi
    if [ "$RET" == 8 ]; then
        exit
    fi

    # browse, edit or run ?
    echo "\n"
    echo "${BLU}[${GRE}0 lynx  ${BLU}] [${GRE}1 vim   ${BLU}] [${GRE}2 sh    ${BLU}]"
    echo "\n"
    echo "${GRE}Select option number and ${CLS}press enter."
    read CMD

    if [ "$CMD" = 2 ]; then
        setup_tools $RET
    else
        check_doc $CMD $RET
    fi

done

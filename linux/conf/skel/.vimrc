" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

"" allow backspacing over everything in insert mode
"set backspace=indent,eol,start
"
if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file (restore to previous version)
  set undofile		" keep an undo file (undo changes after closing)
endif

set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

"" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
"" let &guioptions = substitute(&guioptions, "t", "", "g")
"
"" Don't use Ex mode, use Q for formatting
"map Q gq
"
"" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
"" so that you can undo CTRL-U after inserting a line break.
"inoremap <C-U> <C-G>u<C-U>
"
"" In many terminal emulators the mouse works just fine, thus enable it.
"if has('mouse')
"  set mouse=a
"endif
"
"" Switch syntax highlighting on, when the terminal has colors
"" Also switch on highlighting the last used search pattern.

" colorscheme desert
set t_Co=256
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
  set background=dark
  colorscheme wombat256mod
  " colorscheme desert
  :hi SpellBad ctermfg=Black guifg=Black
endif


"" Only do this part when compiled with support for autocommands.
if has("autocmd")
"
"  " Enable file type detection.
"  " Use the default filetype settings, so that mail gets 'tw' set to 72,
"  " 'cindent' is on in C files, etc.
"  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on
"
"  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!
"
"  " For all text files set 'textwidth' to 78 characters.
"  autocmd FileType text setlocal textwidth=78
"
"  " When editing a file, always jump to the last known cursor position.
"  " Don't do it when the position is invalid or when inside an event handler
"  " (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if line("'\"") >= 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else
  set autoindent		" always set autoindenting on

endif " has("autocmd")

"" Convenient command to see the difference between the current buffer and the
"" file it was loaded from, thus the changes you made.
"" Only define it when not defined already.
"if !exists(":DiffOrig")
"  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
"		  \ | wincmd p | diffthis
"endif
"
"if has('langmap') && exists('+langnoremap')
"  " Prevent that the langmap option applies to characters that result from a
"  " mapping.  If unset (default), this may break plugins (but it's backward
"  " compatible).
"  set langnoremap
"endif

" Search down into subfolders
" Provides tab-completion for all file-related tasks
set path+=**

" Display all matching files when we tab complete
set wildmenu

" Better file browsing
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'


map <F2> :tabnew
map <F3> :tabprevious<CR>
map <F4> :tabnext<CR>
"
"" Show Line Numbers
set relativenumber
set complete=.,b,u,]
set wildmode=longest,list:longest
set completeopt=menu,preview
"
"" Directories
set backupdir=~/.vim/backup
set undodir=~/.vim/undodir
set viewdir=~/.vim/views
set directory=~/.vim/swap
"
"" Spell Check
set spell spelllang=en_us
"
"" Strips whitespace
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>
"
"" Whitespace fixes
highlight ExtraWhitespace ctermbg=red guibg=red

match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()
"
"
"" For tab characters that appear 4-spaces-wide
"set tabstop=4
"" If you're using actual tab character in your source code you probably also 
"" want these settings (these are actually the defaults,set them defensively):
"set softtabstop=0 noexpandtab
"" Finally, if you want an indent to correspond to a single tab, you should also use:
""set shiftwidth=4
"" For indents that consist of 4 space characters but are entered with the tab key:
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
"set list
"" set past gives problems with tabs
"" set paste
""

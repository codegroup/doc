#!/bin/bash

## load client configuration
${IPT} -P INPUT DROP
${IPT} -P FORWARD DROP
${IPT} -P OUTPUT ACCEPT

${IPT} -A INPUT -i lo -s 127.0.0.0/8 -d 127.0.0.0/8 -j ACCEPT
${IPT} -A OUTPUT -o lo -s 127.0.0.0/8 -d 127.0.0.0/8 -j ACCEPT

${IPT} -A INPUT -p tcp --dport 1024:65535 -m state --state ESTABLISHED,RELATED -j ACCEPT
${IPT} -A INPUT -p udp --dport 1024:65535 -m state --state ESTABLISHED,RELATED -j ACCEPT

${IPT} -A OUTPUT  -j ACCEPT

${IPT} -A FORWARD -j LOG --log-level 7 --log-prefix "iptables: FORWARD: "
${IPT} -A INPUT -j LOG --log-level 7 --log-prefix "iptables: INPUT: "

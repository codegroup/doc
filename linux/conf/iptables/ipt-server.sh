echo "setting server iptables ..."

####### Input Chain ######
$IPT -A INPUT -j blocker
$IPT -A INPUT -j blockip_in

$IPT -A INPUT -i ${PUB_IF} -s ${BR_NET} -p udp --dport 520 -j DROP

$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -s ${DNS} -j cli_dns_in
$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -s ${BR_NET} -j cli_http_in
$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -s ${BR_NET} -j cli_https_in
$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -s ${BR_NET} -j cli_ssh_in

$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -s ${BR_NET} -j srv_http_in
$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -s ${BR_NET} -j srv_https_in
$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -s ${BR_NET} -j srv_ssh_in
$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -s ${BR_NET} -j srv_git_in
$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -s ${BR_NET} -j srv_smtp_in

$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -j cli_https_in

$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -j srv_https_in
$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -j srv_http_in
$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -j srv_ssh_in
$IPT -A INPUT -i ${PUB_IF} -d ${PUB_IP} -j srv_git_in

####### Output Chain ######
$IPT -A OUTPUT -j blocker
$IPT -A OUTPUT -j blockip_out

$IPT -A OUTPUT -o ${PUB_IF} -d ${DNS} -s ${PUB_IP} -j cli_dns_out
$IPT -A OUTPUT -o ${PUB_IF} -d ${BR_NET} -s ${PUB_IP} -j cli_http_out
$IPT -A OUTPUT -o ${PUB_IF} -d ${BR_NET} -s ${PUB_IP} -j cli_ssh_out

$IPT -A OUTPUT -o ${PUB_IF} -d ${BR_NET} -s ${PUB_IP} -j srv_https_out
$IPT -A OUTPUT -o ${PUB_IF} -d ${BR_NET} -s ${PUB_IP} -j srv_http_out
$IPT -A OUTPUT -o ${PUB_IF} -d ${BR_NET} -s ${PUB_IP} -j srv_ssh_out
$IPT -A OUTPUT -o ${PUB_IF} -d ${BR_NET} -s ${PUB_IP} -j srv_smtp_out
$IPT -A OUTPUT -o ${PUB_IF} -d ${BR_NET} -s ${PUB_IP} -j srv_git_out

$IPT -A OUTPUT -o ${PUB_IF} -s ${PUB_IP} -j cli_https_out

$IPT -A OUTPUT -o ${PUB_IF} -s ${PUB_IP} -j srv_https_out
$IPT -A OUTPUT -o ${PUB_IF} -s ${PUB_IP} -j srv_http_out
$IPT -A OUTPUT -o ${PUB_IF} -s ${PUB_IP} -j srv_ssh_out
$IPT -A OUTPUT -o ${PUB_IF} -s ${PUB_IP} -j srv_git_out

## log everything else and drop
ipt_log

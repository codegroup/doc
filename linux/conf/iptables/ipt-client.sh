#!/bin/bash

echo "setting dhcp network..."

if [ "${GW}" == "" ]; then

	$IPT -A INPUT  -j srv_icmp
	$IPT -A OUTPUT -j srv_icmp
	$IPT -A INPUT  -j srv_dhcp
	$IPT -A OUTPUT -j srv_dhcp

	echo "no gateway, wating for one..."

	while [ "${GW}" == "" ]
	do
		#we wait for a connection
		sleep 2;
		GW=$(ip route | grep "default via" | cut -d " " -f 3)
	done
fi

echo "setting client network..."

####### Input Chain ######
$IPT -A INPUT -j blocker
$IPT -A INPUT -j blockip_in

$IPT -A INPUT -i ${PUB_IF} -j cli_dns_in
$IPT -A INPUT -i ${PUB_IF} -j cli_http_in
$IPT -A INPUT -i ${PUB_IF} -j cli_https_in
$IPT -A INPUT -i ${PUB_IF} -j cli_git_in
$IPT -A INPUT -i ${PUB_IF} -j cli_ssh_in
$IPT -A INPUT -i ${PUB_IF} -j srv_icmp
$IPT -A INPUT -i ${PUB_IF} -j cli_pops_in
$IPT -A INPUT -i ${PUB_IF} -j cli_smtps_in
$IPT -A INPUT -i ${PUB_IF} -j cli_irc_in
$IPT -A INPUT -i ${PUB_IF} -j cli_ftp_in
$IPT -A INPUT -i ${PUB_IF} -j cli_gpg_in
$IPT -A INPUT -i ${PUB_IF} -p udp --sport 520 --dport 520 -j ACCEPT


####### Output Chain ######
$IPT -A OUTPUT -j blocker
$IPT -A OUTPUT -j blockip_out

$IPT -A OUTPUT -o ${PUB_IF} -j cli_dns_out
$IPT -A OUTPUT -o ${PUB_IF} -j cli_https_out
$IPT -A OUTPUT -o ${PUB_IF} -j cli_ssh_out
$IPT -A OUTPUT -o ${PUB_IF} -j cli_git_out
$IPT -A OUTPUT -o ${PUB_IF} -j cli_git_out
$IPT -A OUTPUT -o ${PUB_IF} -j srv_icmp
$IPT -A OUTPUT -o ${PUB_IF} -j cli_pops_out
$IPT -A OUTPUT -o ${PUB_IF} -j cli_smtps_out
$IPT -A OUTPUT -o ${PUB_IF} -j cli_irc_out
$IPT -A OUTPUT -o ${PUB_IF} -j cli_ftp_out
$IPT -A OUTPUT -o ${PUB_IF} -j cli_gpg_out
$IPT -A OUTPUT -o ${PUB_IF} -p udp --sport 1024:655335 --dport 1024:65535 -j ACCEPT

## log everything else and drop
ipt_log

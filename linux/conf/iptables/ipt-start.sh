#!/bin/bash

echo "starting iptables..."

source /etc/iptables/ipt-firewall.sh

ipt_clear
ipt_tables

# Unlimited on loopback
$IPT -A INPUT -i lo -s 127.0.0.0/8 -d 127.0.0.0/8 -j ACCEPT
$IPT -A OUTPUT -o lo -s 127.0.0.0/8 -d 127.0.0.0/8 -j ACCEPT

case $TYPE in
    bridge)
        #ipt_clear
        echo 1 > /proc/sys/net/ipv4/ip_forward
        source /etc/iptables/ipt-bridge.sh
        exit 0
        ;;
    server)
        #ipt_clear
        source /etc/iptables/ipt-server.sh
        exit 0
        ;;
    client)
        source /etc/iptables/ipt-client.sh
        exit 0
        ;;
    open)
        source /etc/iptables/ipt-open.sh
        exit 0
        ;;
esac

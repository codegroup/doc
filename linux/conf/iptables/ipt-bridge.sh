#!/bin/bash

# Unlimited on loopback
$IPT -A INPUT -i lo -s ${PUB_IP} -d ${PUB_IP} -j ACCEPT
$IPT -A OUTPUT -o lo -s ${PUB_IP} -d ${PUB_IP} -j ACCEPT

#########################################################################
#         FORWARD
#########################################################################

$IPT -A FORWARD -j blocker
$IPT -A FORWARD -j blockip_in
$IPT -A FORWARD -j blockip_out
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -s ${BR_NET} -d ${BR_NET} -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF}  -p udp --dport 520 --sport 520 -j DROP
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -s 0.0.0.0 -d 255.255.255.255 -j srv_dhcp
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -s 10.0.0.4 -d 212.55.154.174 -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -s 10.0.0.4 -d 204.140.20.21 -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -s 10.0.0.4 -d 50.23.197.95 -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -s 10.0.0.4 -d 50.23.197.94 -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -s 10.0.0.4 -d 212.55.154.174 -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -s 10.0.0.4 -d 204.140.20.21 -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -s 10.0.0.4 -d 50.23.197.94 -j ACCEPT

$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -d 10.0.0.4 -s 212.55.154.174 -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -d 10.0.0.4 -s 204.140.20.21 -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -d 10.0.0.4 -s 50.23.197.95 -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -d 10.0.0.4 -s 50.23.197.94 -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -d 10.0.0.4 -s 212.55.154.174 -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -d 10.0.0.4 -s 204.140.20.21 -j ACCEPT
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -d 10.0.0.4 -s 50.23.197.94 -j ACCEPT


$IPT -A FORWARD -i ${BR_IF} -m physdev --physdev-in tap1 --physdev-out ${PUB_IF} -s 10.0.0.3 -j cli_https_out
$IPT -A FORWARD -i ${BR_IF} -m physdev --physdev-in tap1 --physdev-out ${PUB_IF} -s 10.0.0.3 -j cli_http_out
$IPT -A FORWARD -i ${BR_IF} -m physdev --physdev-out tap1 --physdev-in ${PUB_IF} -d 10.0.0.3 -j cli_https_in
$IPT -A FORWARD -i ${BR_IF} -m physdev --physdev-out tap1 --physdev-in ${PUB_IF} -d 10.0.0.3 -j cli_http_in
#####Server
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -m physdev --physdev-in ${PUB_IF} -d 10.0.0.4 -j srv_ssh_in
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -m physdev --physdev-in tap2 -s 10.0.0.4 -j srv_ssh_out
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -m physdev --physdev-in ${PUB_IF} -d 10.0.0.4 -j srv_git_in
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -m physdev --physdev-in tap2 -s 10.0.0.4 -j srv_git_out
#$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -m physdev --physdev-in ${PUB_IF} -d 10.0.0.4 -j srv_ntp

$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -m physdev --physdev-in ${PUB_IF} -d 10.0.0.4 -j cli_http_in
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -m physdev --physdev-in tap2 -s 10.0.0.4 -j cli_http_out
#####HTTP Server
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -m physdev --physdev-in ${PUB_IF} -d 10.0.0.4 -j srv_http_in
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -m physdev --physdev-in tap2 -s 10.0.0.4 -j srv_http_out
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -m physdev --physdev-in ${PUB_IF} -d 10.0.0.4 -j srv_https_in
$IPT -A FORWARD -i ${BR_IF} -o ${BR_IF} -m physdev --physdev-in tap2 -s 10.0.0.4 -j srv_https_out

#########################################################################
#         INPUT
#########################################################################
$IPT -A INPUT -j blocker
$IPT -A INPUT -j blockip_in
$IPT -A INPUT -i ${BR_IF} -p udp --dport 520 --sport 520 -j DROP
$IPT -A INPUT -i ${BR_IF} -d ${PUB_IP} -p tcp  --sport 3030 -j DROP

$IPT -A INPUT -i ${BR_IF} -d ${PUB_IP} -s ${DNS} -j cli_dns_in
$IPT -A INPUT -i ${BR_IF} -m physdev --physdev-in tap2 -s ${BR_NET} -d ${PUB_IP} -j cli_http_in
$IPT -A INPUT -i ${BR_IF} -m physdev --physdev-in ${PUB_IF} -s ${GW} -j cli_http_in
$IPT -A INPUT -i ${BR_IF} -m physdev --physdev-in ${PUB_IF} -j cli_https_in
$IPT -A INPUT -i ${BR_IF} -m physdev --physdev-in tap2 -j cli_https_in
$IPT -A INPUT -i ${BR_IF} -d ${PUB_IP} -s ${BR_NET} -j cli_ssh_in
$IPT -A INPUT -i ${BR_IF} -d ${PUB_IP} -s 10.0.0.4 -j cli_git_in
$IPT -A INPUT -i ${BR_IF} -d ${PUB_IP} -s ${BR_NET} -j srv_dns_in
$IPT -A INPUT -i ${BR_IF} -m physdev --physdev-in ${PUB_IF} -s ${GW} -j srv_dhcp
$IPT -A INPUT -i ${BR_IF} -m physdev --physdev-in ${PUB_IF} -s ${BR_NET} -j srv_dhcp

$IPT -A INPUT -i ${BR_IF} -d ${PUB_IP} -s ${BR_NET} -j srv_ssh_in
#$IPT -A INPUT -j LOG

$IPT -A OUTPUT -o ${BR_IF} -p udp --dport 520 --sport 520 -j DROP

$IPT -A OUTPUT -o ${BR_IF} -s ${PUB_IP} -p tcp --dport 3030 -j DROP
$IPT -A OUTPUT -o blockip_out

$IPT -A OUTPUT -o ${BR_IF} -s ${PUB_IP} -d ${DNS} -j cli_dns_out
$IPT -A OUTPUT -o ${BR_IF} -s ${PUB_IP} -d ${BR_NET} -j srv_dns_out
$IPT -A OUTPUT -o ${BR_IF} -s ${PUB_IP} -d ${BR_NET} -j srv_ssh_out
$IPT -A OUTPUT -o ${BR_IF} -s ${PUB_IP} -d ${BR_NET} -j cli_http_out
$IPT -A OUTPUT -o ${BR_IF} -s ${PUB_IP} -d ${GW} -j cli_http_out
$IPT -A OUTPUT -o ${BR_IF} -s ${PUB_IP} -j cli_https_out
$IPT -A OUTPUT -o ${BR_IF} -s ${PUB_IP} -d ${BR_NET} -j cli_ssh_out
$IPT -A OUTPUT -o ${BR_IF} -s ${PUB_IP} -d 10.0.0.4 -j cli_git_out
$IPT -A OUTPUT -o ${BR_IF} -d ${BR_NET} -j srv_dhcp
$IPT -A OUTPUT -o ${BR_IF} -d ${BR_NET} -j srv_icmp

## log everything else and drop
ipt_log

iptables-save > /etc/iptables/bridge.v4

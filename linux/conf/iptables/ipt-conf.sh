#!/bin/bash

IPT="/usr/sbin/iptables"
IPT6="/usr/sbin/ip6tables"

SPAMLIST="/etc/iptables/blockedip"
SPAMDROPMSG="BLOCKED IP: "

#TYPE=bridge
#TYPE=server
#TYPE=open
TYPE=client

# bridge interface with interface facing gateway
#BR_IF="br0"

# bridge ip network address
BR_NET="10.0.0.0/8"
#BR_NET="192.0.0.0/8"
# static wifi ip network address
#WIFI_NET="10.0.0.0/8"

# network gateway
#GW="10.0.0.1"
GW=$(ip route | grep "default via" | cut -d " " -f 3)

# external dns
#DNS="10.0.0.254"
#DNS=$(grep -m 1 "nameserver " /etc/resolv.conf | cut -d " " -f 2)
DNS=$(grep -m 1 "^server=" /etc/dnsmasq.conf | cut -d "=" -f 2)

# public interface facing gateway
#PUB_IF="wlp9s0"
PUB_IF="enp7s0"

# wifi interface
#WIFI_IF="wlp7s0"

# static machine ip address
#PUB_IP="10.0.0.4"
# deserve much better :(
PUB_IP=$(ifconfig ${PUB_IF} | grep -m 1 "addr:" | cut -d " " -f 12 | cut -d ":" -f 2)

echo "GW: ${GW}"
echo "DNS: ${DNS}"
echo "PUB_IF: ${PUB_IF}"
echo "PUB_IP: ${PUB_IP}"

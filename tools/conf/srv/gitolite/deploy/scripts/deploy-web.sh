#!/bin/sh

pkg_path=$1

www_root="/var/www/htdocs"
www_user="www"
www_group="daemon"

pkg_file="${pkg_path}/project"
pkg_rm="${pkg_path}/deleted"
pkg_files="${pkg_path}/files"

if [ ! -f ${pkg_file} ]; then
    echo "Deploy web: invalid pkg_file ${pkg_file}"
    exit 1
fi

pkg_name=$(head -1 ${pkg_file})
pkg_new=$(head -3 ${pkg_file} | tail -1)
pkg_new7=$(echo ${pkg_new} | cut -c1-7)

pkg_www="${www_root}/${pkg_name}.example.com"
pkg_back="${pkg_www}/backup_deploy"
pkg_last="${pkg_www}/.last_deploy"

if [ ! -d ${pkg_www} ]; then
    echo "Deploy web: invalid pkg_www ${pkg_www}"
    exit 1
fi

# first backup all data
if [[ ! $(ls ${pkg_www} | grep -v "backup_deploy") = "" ]]; then
    if [ ! -d ${pkg_back} ]; then
    	mkdir -p ${pkg_back}
    fi
    backup_file="${pkg_back}/${pkg_name}-$(date '+%Y-%j-%H-%M-%S').tar.gz"
    echo "Deploy web: making backup ${backup_file}"
    tar --exclude ${pkg_back} --xattrs -zcpf ${backup_file} ${pkg_www}
    chown -R ${www_user}:${www_group} ${pkg_back}
fi

# remove files and directories that have been deleted
if [ -f ${pkg_rm} ]; then

    echo "Deploy web: files to delete:"
    # first we delete files
    while read deleted_file; do
        deleted_file="${pkg_www}/${deleted_file}"
        if [ -f ${deleted_file} ]; then
            echo "file      rm ${deleted_file}"
            rm ${deleted_file}
        fi
    done <${pkg_rm}

    # delete directories
    while read deleted_file; do
        deleted_file="${pkg_www}/${deleted_file}"
        if [ -d ${deleted_file} ]; then
            echo "file      rm ${deleted_file}"
            rm ${deleted_file}
        fi
    done <${pkg_rm}

fi

# copy new files to destination
if [ -d ${pkg_files} ]; then
    echo "Deploy web: cp from ${pkg_files} to ${pkg_www}"
    cp -r ${pkg_files}/* ${pkg_www}
    chown -R ${www_user}:${www_group} ${pkg_www} 
fi

echo ${pkg_new} > ${pkg_last}
echo "Deploy: scripts/deployweb.sh ${pkg_name} ${pkg_new7} deployed."

#remove temporary package
rm -r ${pkg_path}
